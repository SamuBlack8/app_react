import React from 'react';
import { objToPost } from '../../models/Post';
import { withRouter } from 'react-router-dom';
import { Link } from "react-router-dom";

const url = 'http://laragon.test/bedrock/web/wp-json/wp/v2/posts?categories='

class AllCategories extends React.Component {
	constructor() {
		super();

		this.state = {
			categoriesReact: []
		};
	}

	componentDidMount() {
		this.handleFetchCategories(this.props.match.params.id);
	}

	componentDidUpdate(prevProps) {
		const hasUrlChanged = prevProps.match.params.id !== this.props.match.params.id;

		if (hasUrlChanged) {
			this.handleFetchCategories(this.props.match.params.id);
		}
	}

	handleFetchCategories(id) {
		fetch(url + id)
			.then(res => res.json())
			.then(categories => {
				this.setState({
					categoriesReact: categories.map(category => objToPost(category))
				});
			});
	}

	render() {
		const items = this.state.categoriesReact.map(c =>
			<div key={c.id} className="col">
				<div className="card" style={{ width: "18rem" }}>
					<div className="card-body">
						<h4>{c.id} - {c.title}</h4>
						<p dangerouslySetInnerHTML={{ __html: c.excerpt }} className="card-text"></p>
					</div>
					<Link to={'/posts/' + c.id} className="btn btn-outline-info">Leggi tutto</Link>
				</div>
			</div>
		);

		return (
			<div className="container-fluid">
				<div className="row row-cols-1 row-cols-md-3 g-4">
					{items.length ? items : <h1>No items here..</h1>}
				</div>
			</div>
		);
	}
}

export default withRouter(AllCategories);