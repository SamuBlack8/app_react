import React from 'react';

class NotFound extends React.Component {
	render() {
		return (
			<div className="alert alert-warning" role="alert">
				404 - Page not Found!
			</div>
		);
	}
}

export default NotFound;