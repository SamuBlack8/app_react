import { scryRenderedComponentsWithType } from "react-dom/test-utils";
import { objToPost } from '../../models/Post';
import React from 'react';
import { Link } from "react-router-dom";
// import { objToPost } from '../../models/Post';
// import PostsListRow from '../PostsListRow';

const url = 'http://laragon.test/bedrock/web/wp-json/wp/v2/posts/'

export default class PostsList extends React.Component {
  constructor() {
    super();

    this.state = {
      articles: []
    };
  }

  componentDidMount() {
    fetch(url)
      .then(res => res.json())
      .then(posts => this.setState({
        articles: posts.map(post => objToPost(post))
      })
    );
  }

  render() {
    const items = this.state.articles.map(go => 
      <div key={go.id} className="col"> 
        <div className="card" style={{width: "18rem"}}>
          <div className="card-body">
            <h4>{go.id} - {go.title}</h4>
            <p dangerouslySetInnerHTML={{__html:go.excerpt}} className="card-text"></p>
          </div>
          <Link to={'/posts/'+go.id} className="btn btn-outline-info">Leggi tutto</Link>
        </div>
      </div>
    );

    return (
      <div className="container-fluid">
        <div className="row row-cols-1 row-cols-md-3 g-4">
          {items}
        </div>
      </div>
    );
  }
}