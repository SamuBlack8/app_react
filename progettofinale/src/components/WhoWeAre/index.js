import { scryRenderedComponentsWithType } from "react-dom/test-utils";
import { objToPage } from '../../models/Page';
import React from 'react';
import { withRouter } from "react-router";
// import { objToPost } from '../../models/Post';
// import PostsListRow from '../PostsListRow';

const url = 'http://laragon.test/bedrock/web/wp-json/wp/v2/pages/'

class WhoWeAre extends React.Component {
	constructor() {
		super();

		this.state = {
			whoWeAre: null
		};
	}

	componentDidMount() {
		fetch(url + this.props.match.params.id)
			.then(res => res.json())
			.then(who => {
				this.setState({
					whoWeAre: objToPage(who)
				})
			});
	}

	render() {
		const item = this.state.whoWeAre
			? (
				<section>
					<h3>{this.state.whoWeAre.title}</h3>
					<article>
						<p dangerouslySetInnerHTML={{ __html: this.state.whoWeAre.content }}></p>
					</article>
				</section>
			)
			: (
				null
			);

		return (
			<div className="container-fluid">
				{item || <h1>Loading...</h1>}
			</div>
		);
	}
}

export default withRouter(WhoWeAre)