import React from "react";
import { withRouter } from "react-router";

const url = 'http://laragon.test/bedrock/web/wp-json/wp/v2/posts/'

class PostListRow extends React.Component {

	constructor() {
		super();

		this.state = {
			post: null
		};
	}

	componentDidMount() {
		fetch(url + this.props.match.params.id)
			.then(res => res.json())
			.then(data => {
				this.setState({ post: data })
			});
	}

	render() {
		if (!this.state.post) {
			return (
				<h1>Loading...</h1>
			);
		}

		return (
			<div>
				<h1>{this.state.post.title.rendered}</h1>
				<div dangerouslySetInnerHTML={{ __html: this.state.post.content.rendered }} />
			</div>
		);
	}
}

export default withRouter(PostListRow)