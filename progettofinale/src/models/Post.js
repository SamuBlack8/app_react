export class Post {
	constructor(id, title, excerpt) {
		this.excerpt = excerpt;
		this.id = id;
		this.title = title;
	}
}

export function objToPost(obj) {
	return new Post(obj.id, obj.title.rendered, obj.excerpt.rendered);
}