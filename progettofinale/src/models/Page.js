export class Page {
	constructor(id, title, content) {
		this.content = content;
		this.id = id;
		this.title = title;
	}
}

export function objToPage(obj) {
	return new Page(obj.id, obj.title.rendered, obj.content.rendered);
}

