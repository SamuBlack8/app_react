import './App.css';
import PostsList from './components/PostsList';
import WhoWeAre from './components/WhoWeAre';
import NotFound from './components/NotFound';
import AllCategories from './components/AllCategories';
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';
import PostListRow from './components/PostListRow';

function App() {
  return (
    //header
    <div>
      <img src="../assets/img/loghi.jpg" className="ms-5 m-4" alt="logo" width="230" height="100" />
      <div className="bg-dark.bg-gradient text-dark">
        <div className="container-fluid fakeWarning">
          <BrowserRouter>
            <nav className="navbar navbar-expand-lg navbar-light fakeInfo">
              <div className="container-fluid">
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                  <ul className="navbar-nav">
                    <li className="nav-item">
                      <Link to="/" className="nav-link active fw-bold aNavB">Home</Link>
                    </li>
                    <li className="nav-item">
                      <Link to="/chi-siamo/5" className="nav-link fw-bold aNavB">Chi siamo</Link>
                    </li>
                    <li className="nav-item">
                      <Link to="/categoria-react/2" className="nav-link fw-bold aNavB">React</Link>
                    </li>
                    <li className="nav-item">
                      {/* <Link to="/categoria-react/3" className="nav-link fw-bold">Categoria Wp</Link> */}
                      <Link to="/categoria-react/3" className="nav-link fw-bold aNavB">Wp</Link>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
            <div className="row">
              <div className="col-lg-12">
                <Switch>
                  <Route path="/" exact>
                    <PostsList />
                  </Route>
                  <Route path="/chi-siamo/:id">
                    <WhoWeAre />
                  </Route>
                  <Route path="/categoria-react/:id">
                    <AllCategories />
                  </Route>
                  <Route path="/posts/:id">
                    <PostListRow />
                  </Route>
                  <Route path="*" >
                    <NotFound />
                  </Route>
                </Switch>
              </div>
            </div>
          </BrowserRouter>
        </div>
      </div>
      <div>
        {/* footer */}
        <footer className="text-center text-white" style={{ backgroundColor: "#f1f1f1" }}>
          {/* <!-- Grid container --> */}
          <div className="container pt-4">
            {/* <!-- Section: Social media --> */}
            <section className="mb-4">
              {/* <!-- Facebook --> */}
              <a
                className="btn btn-link btn-floating btn-lg text-dark m-1"
                href="#!"
                role="button"
                data-mdb-ripple-color="dark">
                <i className="fab fa-facebook-f"></i>
              </a>

              {/* <!-- Twitter --> */}
              <a
                className="btn btn-link btn-floating btn-lg text-dark m-1"
                href="#!"
                role="button"
                data-mdb-ripple-color="dark"
              ><i className="fab fa-twitter"></i>
              </a>

              {/* <!-- Google --> */}
              <a
                className="btn btn-link btn-floating btn-lg text-dark m-1"
                href="#!"
                role="button"
                data-mdb-ripple-color="dark"
              ><i className="fab fa-google"></i>
              </a>

              {/* <!-- Instagram --> */}
              <a
                className="btn btn-link btn-floating btn-lg text-dark m-1"
                href="#!"
                role="button"
                data-mdb-ripple-color="dark"
              ><i className="fab fa-instagram"></i>
              </a>

              {/* <!-- Linkedin --> */}
              <a
                className="btn btn-link btn-floating btn-lg text-dark m-1"
                href="#!"
                role="button"
                data-mdb-ripple-color="dark"
              ><i className="fab fa-linkedin"></i>
              </a>
              {/* <!-- Github --> */}
              <a
                className="btn btn-link btn-floating btn-lg text-dark m-1"
                href="#!"
                role="button"
                data-mdb-ripple-color="dark"
              ><i className="fab fa-github"></i>
              </a>
            </section>
            {/* <!-- Section: Social media --> */}
          </div>
          {/* <!-- Grid container --> */}

          {/* <!-- Copyright --> */}
          <div className="text-center text-dark p-3" style={{ backgroundColor: "rgba(0, 0, 0, 0.2)" }}>
            © 2020 Copyright:
            <a className="text-dark" href="https://mdbootstrap.com/">MDBootstrap.com</a>
          </div>
          {/* <!-- Copyright --> */}
        </footer>
      </div>
    </div>
  );
}

export default App;

